TRMS v4 Description:

TRMS, or Tuition Reimbursement Management System is a full-stack web application that allows employees to submit requests for reimbursements for courses, events, and certifications. These requests can then be approved or rejected by the employee's direct supervisor, department head, and a benefits coordinator while the employee is able to track the status of their requests.

Technologies Used:

     HTML, Maven, CSS, JavaScript, Git, Hibernate, Java, PostgreSQL

Features:

     Employee can apply for tution Reimbursement by sending the new applicaiton. Employee can check their application status. In the application process, application need to approval from the different department like supervisor, department head and benifit coordinator..

Getting Started

    git remote add origin https://gitlab.com/shamimRevature/project1

    compile and run the java code for the server and database connection
    open up the index.html in the browser and pick a login

Usage

    You open index.html to get a screen that you have login
